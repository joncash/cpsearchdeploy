﻿/*  
 *Home/Plan 
 */
/* OLD
var $CollapseBar = {
	"this": $("#collapse-bar"),
	"showClose": "glyphicon-chevron-left",
	"showOpen": "glyphicon-chevron-right",	
};
*/
var $CollapseBar = {
	"this": $("#collapse-bar"),
	"showClose": "search-container-hide",
	"showOpen": "search-container-show",
};
$(function () {
	$("#collapse-bar").click(function () {
		var $this = $(this);
		var searchContainerOpen = $this.hasClass($CollapseBar.showOpen);//
		console.log("searchContainerOpen : " + searchContainerOpen);
		if (searchContainerOpen) {
			//toggle to ShowOpen
			//collaspePatenSearch();
			hideSearch();
		}
		else {
			//toggle to ShowClose
			//expandPatentSearch();
			showSearch();
		}
		draw_TECH_DIST_Chart();
	});
});

function hideSearch() {
	var $this = $CollapseBar.this;
	$this.removeClass($CollapseBar.showOpen).addClass($CollapseBar.showClose);
	$("#searchContainer").hide();
	$("#planContainer").removeClass("col-md-7").addClass("col-md-12");
	console.log('hideSearch');
}
function showSearch() {
	var $this = $CollapseBar.this;
	$this.removeClass($CollapseBar.showClose).addClass($CollapseBar.showOpen);
	$("#searchContainer").show("200");
	$("#planContainer").removeClass("col-md-12").addClass("col-md-7");
	console.log('showSearch');
}

function collaspePatenSearch() {
	var showClose = $CollapseBar.showClose;
	var showOpen = $CollapseBar.showOpen;
	var $this = $CollapseBar.this;
	//toggle to ShowOpen
	$this.removeClass(showClose).addClass(showOpen);

	/**/
	$("#searchContainer").removeClass("col-md-5").addClass("col-md-1").children("#patentSearch").hide();
	$("#planContainer").removeClass("col-md-7").addClass("col-md-11");
	$this.attr("title", "展開「檢索頁面」");

}
function expandPatentSearch() {
	var showClose = $CollapseBar.showClose;
	var showOpen = $CollapseBar.showOpen;
	var $this = $CollapseBar.this;
	//toggle to ShowClose
	$this.removeClass(showOpen).addClass(showClose);
	/**/
	$("#searchContainer").removeClass("col-md-1").addClass("col-md-5").children("#patentSearch").show();
	$("#planContainer").removeClass("col-md-11").addClass("col-md-7");
	$this.attr("title", "收合「檢索頁面」");

}

function initNewPlan() {
	$CollapseBar.this.hide();
	collaspePatenSearch();
}

/*==================================
 * _PlanPTAD3Partial
 * 標的專利技術分佈圖
 * 
 * 
 ===================================*/

$(function () {
	$(".fn-toggle").click(function () {
		var connClass = $(this).attr("connect");
		$("." + connClass).toggleClass("hide");
	});

	//export SVG
	bindExportSVGToPNG();
	bindExportSVG();

	//
	$(window).resize(function () {
		draw_TECH_DIST_Chart();
	});

});

//Draw D3 Example
function draw_TECH_DIST_Chart() {
	var data = $("#svg-useD3-container").data("d3-data");
	/*
	//Example Data
	var data = [
		{ deep: "4", wide: "14", species: "US 8,512,056" },
		{ deep: "16", wide: "8", species: "US 8,388,352" },
		{ deep: "10", wide: "3", species: "US 8,360,788" },
		{ deep: "10", wide: "2", species: "US 8,197,267" },
		{ deep: "18", wide: "5", species: "US 7,740,484" },
		{ deep: "49", wide: "18", species: "US 6,068,490" },
		{ deep: "11", wide: "3", species: "US 5,711,674" },
		{ deep: "-9", wide: "7", species: "US 4,753,600" },
		{ deep: "-10", wide: "26", species: "US 4,590,337" },
	];
	*/
	if (data == null || data.length == 0) return;

	//定義 SVG 大小
	var margin = { "top": 40, "right": 180, "bottom": 60, "left": 40 };
	/**/
	var $planContainer = $("#planContainer");
	var width = $planContainer.width() - margin.left - margin.right;
	var height = window.innerHeight - margin.top - margin.bottom - 200;
	/*
	var width = window.innerWidth - margin.left - margin.right;
	var height = window.innerHeight - margin.top - margin.bottom - 200;
	*/

	//設定 x, y 方向比例尺
	var x = d3.scale.linear()
		.range([0, width]);

	var y = d3.scale.linear()
		.range([height, 0]);

	//設定可用顏色種類
	var color = d3.scale.category10();

	//定義 x, y 軸
	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom");
	var xAxisText = "技術廣度 (BOT)";

	var yAxis = d3.svg.axis()
		.scale(y)
		.orient("left");
	var yAxisText = "技術深度 (DOT)";

	//加入 SVG 圖形位置
	var svgW = $("#svg-useD3-container").width() - 20;//"100%";//width + margin.left + margin.right;
	var svgH = height + margin.top + margin.bottom;

	//Clear	
	$("#svg-main").find("g").remove();

	//append Graphic
	var svg = d3.select("#svg-main")
		.attr("width", svgW)
		.attr("height", svgH)
		.attr("style", "background-color:white")
		.attr("class", "svg-main")
		.append("g")
		.attr("class", "svg-body")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	data.forEach(function (d) {
		d.deep = +d.deep;
		d.wide = +d.wide;
	});

	//定義 x,y 軸方法
	x.domain(d3.extent(data, function (d) { return d.wide; })).nice();
	y.domain(d3.extent(data, function (d) { return d.deep; })).nice();

	//X軸
	svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(" + 0 + "," + y(0) + ")")
		.attr("fill-opacity", 0.5)
        .call(xAxis)
      .append("text")
        .attr("class", "label")
        .attr("x", width)
        .attr("y", -6)
        .style("text-anchor", "end")
        .text(xAxisText);

	//y軸
	svg.append("g")
        .attr("class", "y axis")
		.attr("fill-opacity", 0.5)
        .call(yAxis)
      .append("text")
        .attr("class", "label")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", ".71em")
        .style("text-anchor", "end")
        .text(yAxisText);


	var top25AreaData = getTop25AreaData(data);

	/*
	*畫 TOP 25% Deep Area
	* 座標點為 (maxWide, maxDeep)
	* Width = maxWide - minWide 
	* Height = maxDeep - top25AvgDeep
	*/
	drawDeepArea(svg, top25AreaData, x, y);

	/*
	*畫 TOP 25% Wide Area
	* 座標點 (x,y) =  (top25AvgWide, maxDeep)
	* Width = maxWide - top25AvgWide 
	* Height = maxDeep - minDeep
	*/
	drawWideArea(svg, top25AreaData, x, y);

	/*
	*畫 Cross Area ( Wide Area and Deep Area)
	* 座標點 (x,y) =  (top25AvgWide, maxDeep)
	* Width = maxWide - top25AvgWide 
	* Height = maxDeep - top25AvgDeep
	*/
	drawCrossArea(svg, top25AreaData, x, y);

	/*畫迴歸線
	* 最小平方法
	* Deep & Wide > 0
	*/
	drawRegressionLine(svg, data, top25AreaData, x, y);


	/*畫座標點*/
	var patContainer = svg.append("g")
        .attr("class", "patentPointContainer")
        .attr("transform", "translate(0,0)");

	var radius = 3.5; //圓點大小
	patContainer.selectAll(".dot")
        .data(data)
		.enter()
		.append("circle")
        .attr("class", "dot")
		.attr("r", radius)
        .attr("r", radius)
        .attr("cx", function (d) { return x(d.wide); })
        .attr("cy", function (d) { return y(d.deep); })
		.attr("data-deep", function (d) { return d.deep; })
		.attr("data-wide", function (d) { return d.wide; })
        .style("fill", function (d) { return color(d.species); });


	//替每一個圓加上文字
	patContainer.selectAll(".text")
       .data(data)
        .enter().append("text")
		.attr("class", "pn-text")
        .attr("x", function (d) { return x(d.wide); })
        .attr("y", function (d) { return y(d.deep); })
        .text(function (d) { return d.species; })
        .attr("font-family", "sans-serif")
        .attr("font-size", "12px")
        .attr("fill", "black")
		.append("title")
		.text(function (d) {
			var disp = "(depth, breadth) = ";
			disp += " (" + d.deep + ", " + d.wide + ")";
			return disp;
		});;

	/*畫圖例*/
	var legend = svg.selectAll(".legend")
        .data(color.domain())
		.enter().append("g")
        .attr("class", "legend")
        .attr("transform", function (d, i) { return "translate(150," + i * 20 + ")"; });


	legend.append("rect")
        .attr("x", width - 135)
        .attr("width", 18)
        .attr("height", 18)
        .style("fill", color);

	legend.append("text")
        .attr("x", width - 24)
        .attr("y", 9)
        .attr("dy", ".35em")
        .style("text-anchor", "end")
        .text(function (d) { return d; });
}

/*
 * 畫 Top 25 區域資料處理
 */
var precisionD = Math.pow(10, 6);//計算精度小數第 6 位

function getTop25AreaData(data) {
	var dataCount = data.length;

	//取得 Data 的最大，最小值, top 25 
	var INTMin = -32767;
	var INTMax = 32767;
	var deepData = [];
	var wideData = [];
	var maxDeep = INTMin;
	var minDeep = INTMax;
	var maxWide = INTMin;
	var minWide = INTMax;

	for (var idx = 0; idx < dataCount; idx++) {
		var deep = +data[idx].deep;
		var wide = +data[idx].wide
		deepData.push(deep);
		wideData.push(wide);
		if (deep >= maxDeep) maxDeep = deep;
		if (deep <= minDeep) minDeep = deep;
		if (wide >= maxWide) maxWide = wide;
		if (wide <= minWide) minWide = wide;
	}

	var sortedDeep = deepData.sort(function (prev, curr) { return prev < curr ? 1 : -1 });
	var sortedWide = wideData.sort(function (prev, curr) { return prev < curr ? 1 : -1 });
	var top25AvgDeep = getTop25Avg(sortedDeep);
	var top25AvgWide = getTop25Avg(sortedWide);

	var top25AreaData = [{
		"maxWide": maxWide,
		"minWide": minWide,
		"maxDeep": maxDeep,
		"minDeep": minDeep,
		"top25AvgDeep": top25AvgDeep,
		"top25AvgWide": top25AvgWide
	}];

	return top25AreaData;
}//end

function getTop25Avg(data) {
	//Top 25%
	var top25Idx = Math.ceil(data.length * 0.25);
	var sum = 0;
	for (var idx = 0; idx < top25Idx; idx++) {
		sum += +data[idx];
	}
	var avg = Math.round(sum / top25Idx * precisionD) / precisionD;
	return avg;
}//end


/*
 * 畫 Top 25 區域
 */
function drawDeepArea(svg, top25AreaData, xFunc, yFunc) {

	var className = "top25DeepArea";
	var fillColor = '#fcf8e3';//yellow based

	var rectWidth = Math.abs(xFunc(top25AreaData[0].maxWide) - xFunc(top25AreaData[0].minWide));
	var rectHeight = Math.abs(yFunc(top25AreaData[0].top25AvgDeep) - yFunc(top25AreaData[0].maxDeep));
	var upperLeft = { "x": xFunc(top25AreaData[0].minWide), "y": yFunc(top25AreaData[0].maxDeep) };

	var container = svg.append("g")
        .attr("class", "drawDeepAreaContainer")
		.attr("transform", "translate(" + 0 + "," + 0 + ")");
	//
	container.selectAll("." + className)
		.data(top25AreaData)
		.enter()
		.append('rect')
		.attr("class", className)
		.attr(
		{
			'fill': fillColor,
			'fill-opacity': 0.5,
			'width': rectWidth,
			'height': rectHeight,
			'x': upperLeft.x,
			'y': upperLeft.y
		});
	//畫斜線右上到左下		
	drawDeepAreaSlash(container, rectWidth, rectHeight, upperLeft);
}

function drawWideArea(svg, top25AreaData, xFunc, yFunc) {
	/*
	*畫 TOP 25% Wide Area
	* 座標點 (x,y) =  (top25AvgWide, maxDeep)
	* Width = maxWide - top25AvgWide 
	* Height = maxDeep - minDeep
	*/
	var fillColor = '#d9edf7';//blue based
	var className = "top25WideArea";

	var rectWidth = Math.abs(xFunc(top25AreaData[0].maxWide) - xFunc(top25AreaData[0].top25AvgWide));
	var rectHeight = Math.abs(yFunc(top25AreaData[0].minDeep) - yFunc(top25AreaData[0].maxDeep));
	var upperLeft = { "x": xFunc(top25AreaData[0].top25AvgWide), "y": yFunc(top25AreaData[0].maxDeep) };
	//console.log(upperLeft);

	var container = svg.append("g")
        .attr("class", "drawWideAreaContainer")
		.attr("transform", "translate(" + 0 + "," + 0 + ")");

	container.selectAll("." + className)
		.data(top25AreaData)
		.enter()
		.append('rect')
		.attr("class", className)
		.attr(
		{
			'fill': fillColor,
			'fill-opacity': 0.5,
			'width': rectWidth,
			'height': rectHeight,
			'x': upperLeft.x,
			'y': upperLeft.y
		});

	//draw slash
	drawWideAreaSlash(container, rectWidth, rectHeight, upperLeft);
}

function drawCrossArea(svg, top25AreaData, xFunc, yFunc) {
	/*
	*畫 Cross Area ( Wide Area and Deep Area)
	* 座標點 (x,y) =  (top25AvgWide, maxDeep)
	* Width = maxWide - top25AvgWide 
	* Height = maxDeep - top25AvgDeep
	*/
	var fillColor = '#dff0d8';//green based
	var className = "top25CrossArea";
	var data = top25AreaData[0];
	var rectWidth = Math.abs(xFunc(data.maxWide) - xFunc(data.top25AvgWide));
	var rectHeight = Math.abs(yFunc(data.top25AvgDeep) - yFunc(data.maxDeep));
	var upperLeft = {
		"x": xFunc(data.top25AvgWide),
		"y": yFunc(data.maxDeep)
	};

	var container = svg.append("g")
        .attr("class", "drawWideAreaContainer")
		.attr("transform", "translate(" + 0 + "," + 0 + ")");

	//
	container.selectAll("." + className)
		.data(top25AreaData)
		.enter()
		.append('rect')
		.attr("class", className)
		.attr(
		{
			'fill': fillColor,
			'fill-opacity': 0.5,
			'width': rectWidth,
			'height': rectHeight,
			'x': upperLeft.x,
			'y': upperLeft.y
		});
}

/*
 * 畫 Top 25 區域的斜線
 */
function drawDeepAreaSlash(svg, rectWidth, rectHeight, upperLeft) {
	//assign
	var slashAry = [];
	var N = 22;//slash count
	var dw = rectWidth / (N + 1);
	var dh = rectHeight / (N + 1);

	//右上到左下:對角線
	slashAry.push({
		"x1": upperLeft.x + rectWidth,
		"y1": upperLeft.y,
		"x2": upperLeft.x,
		"y2": upperLeft.y + rectHeight
	});
	//console.log(slashAry);

	/**/
	//對角線右邊區域
	for (var idx = 1; idx <= N; idx++) {
		//右上座標
		var x1 = upperLeft.x + rectWidth;
		var y1 = upperLeft.y + (dh * idx);

		//左下座標
		x2 = upperLeft.x + (dw * idx);
		y2 = upperLeft.y + rectHeight;
		slashAry.push({ "x1": x1, "y1": y1, "x2": x2, "y2": y2 });
	}

	/**/
	//對角線左邊區域
	for (var idx = 1; idx <= N; idx++) {
		//右上座標
		var x1 = upperLeft.x + (dw * idx);
		var y1 = upperLeft.y;

		//左下座標
		x2 = upperLeft.x;
		y2 = upperLeft.y + (dh * idx);;
		slashAry.push({ "x1": x1, "y1": y1, "x2": x2, "y2": y2 });
	}

	//console.log(slashAry);
	var style = 'stroke:rgb(250,220,80);stroke-width:2;';

	svg.selectAll(".slash_deeparea")
		.data(slashAry)
		.enter()
		.append('line')
		.attr("class", "slash_deeparea")
		.attr(
		{
			'style': style,
			'x1': function (d) { return d.x1; },
			'y1': function (d) { return d.y1; },
			'x2': function (d) { return d.x2; },
			'y2': function (d) { return d.y2; }
		});
}

function drawWideAreaSlash(svg, rectWidth, rectHeight, upperLeft) {
	//assign
	var slashAry = [];
	var N = 22;//slash count
	var dw = rectWidth / (N + 1);
	var dh = rectHeight / (N + 1);

	//左上到右下:對角線
	slashAry.push({ "x1": upperLeft.x, "y1": upperLeft.y, "x2": upperLeft.x + rectWidth, "y2": upperLeft.y + rectHeight });

	//對角線右邊區域
	for (var idx = 1; idx <= N; idx++) {
		//左上座標
		var x1 = upperLeft.x + (dw * idx);
		var y1 = upperLeft.y;

		//右下座標
		x2 = upperLeft.x + rectWidth;
		y2 = upperLeft.y + rectHeight - (dh * idx);
		slashAry.push({ "x1": x1, "y1": y1, "x2": x2, "y2": y2 });
	}

	//對角線左邊區域
	for (var idx = 1; idx <= N; idx++) {
		//左上座標
		var x1 = upperLeft.x;
		var y1 = upperLeft.y + (dh * idx);

		//右下座標
		x2 = upperLeft.x + rectWidth - (dw * idx);
		y2 = upperLeft.y + rectHeight;
		slashAry.push({ "x1": x1, "y1": y1, "x2": x2, "y2": y2 });
	}
	//console.log(slashAry);

	svg.selectAll(".slash_widearea")
		.data(slashAry)
		.enter()
		.append('line')
		.attr("class", "slash_widearea")
		.attr(
		{
			'style': 'stroke:rgb(88,192,246);stroke-width:2;',
			'x1': function (d) { return d.x1; },
			'y1': function (d) { return d.y1; },
			'x2': function (d) { return d.x2; },
			'y2': function (d) { return d.y2; }
		});
}

/*
 * 畫趨勢線
 */
function drawRegressionLine(svg, data, top25AreaData, xFunc, yFunc) {
	//
	var regressionData = getRegressionData(data);
	var positiveD = regressionData.positive;
	var negativeD = regressionData.negative;

	var positiveRegressionLine = getTrendLineModel(positiveD, top25AreaData);
	var negativeRegressionLine = getTrendLineModel(negativeD, top25AreaData);
	//console.log(positiveRegressionLine);

	var trendLineSVG = svg.append("g")
		.attr("class", "trendLineContainer")
		.attr("transform", "translate(0,0)");

	var reg1Style = 'stroke:rgb(0,255,0);stroke-width:2;';//green
	var reg2Style = 'stroke:rgb(250,100,0);stroke-width:2;';//red

	//趨勢線至少要 2 個點資料
	if (positiveD.length > 1) {
		drawSVGLine(trendLineSVG, positiveRegressionLine, "positiveLine", reg1Style, xFunc, yFunc);
	}
	if (negativeD.length > 1) {
		drawSVGLine(trendLineSVG, negativeRegressionLine, "negativeLine", reg2Style, xFunc, yFunc);
	}
}//end

function drawSVGLine(svg, lineData, className, style, xFunc, yFunc) {
	svg.selectAll("." + className)
		.data(lineData)
		.enter()
		.append('line')
		.attr("class", className)
		.attr(
		{
			'style': style,
			'x1': function (d) { return xFunc(d.x1); },
			'y1': function (d) { return yFunc(d.y1); },
			'x2': function (d) { return xFunc(d.x2); },
			'y2': function (d) { return yFunc(d.y2); }
		});
}//end

/*
 * 畫趨勢線資料處理
 */
function getRegressionData(data) {
	var positive = [];
	var negative = [];
	for (var idx = 0; idx < data.length; idx++) {
		if (data[idx].deep > 0 && data[idx].wide) {
			positive.push(data[idx]);
		}
		else {
			negative.push(data[idx]);
		}
	}
	return { positive, negative};
	}//end

function getTrendLineModel(regressionD, top25AreaData) {
	var avgData = getAverageData(regressionD);
	var avgX = avgData.AvgX;
	var avgY = avgData.AvgY;

	var beta = CalcBeta(regressionD, avgX, avgY);
	var alpha = CalcAlpha(avgX, avgY, beta);

	var trendLine =[{
		"x1": top25AreaData[0].minWide,
		"y1": CalcY(top25AreaData[0].minWide, alpha, beta),
		"x2": top25AreaData[0].maxWide,
		"y2": CalcY(top25AreaData[0].maxWide, alpha, beta),
	}];
	return trendLine;
	}//end

function getAverageData(data) {
	var sumWide = 0.0;
	var sumDeep = 0.0;
	var dataCount = data.length;
	for (var idx = 0; idx < dataCount; idx++) {
		var deep = +data[idx].deep;
		var wide = +data[idx].wide;
		sumWide += wide;
		sumDeep += deep;
	}
	var avgData = {
		"AvgX": Math.round(sumWide / dataCount * precisionD) / precisionD,
		"AvgY": Math.round(sumDeep / dataCount * precisionD) / precisionD
	};
	return avgData;
}//end

/*
 * 趨勢線計算公式 
 * Y = alpha + beta * X
 */
function CalcBeta(data, avgX, avgY) {
	//分子
	var sum1 = 0.0;

	//分母
	var sum2 = 0.0;
	for (var idx = 0; idx < data.length; idx++) {
		var x = +data[idx].wide;
		var y = +data[idx].deep;
		sum1 += (x - avgX) * (y - avgY);
		sum2 += Math.pow((x - avgX), 2);
	}
	var beta = Math.round(sum1 / sum2 * precisionD) / precisionD;
	return beta;
}//end

function CalcAlpha(avgX, avgY, beta) {
	var realAlpha = avgY - (beta * avgX);
	var alpha = Math.round(realAlpha * precisionD) / precisionD;
	return alpha;
}

function CalcY(x, alpha, beta) {
	return Math.round(alpha + beta * x * precisionD) / precisionD;
}

/*
 * 匯出 SVG to PNG
 */
function bindExportSVGToPNG() {
	d3.select("#exportToPNG").on("click", function () {
		var html = d3.select("svg")
			  .attr("version", 1.1)
			  .attr("xmlns", "http://www.w3.org/2000/svg")
			  .node().parentNode.innerHTML;

		//console.log(html);

		//把 SVG 轉換為 base64 字串，嵌在 img 上，中文會有編碼問題
		/*
		  * 較新的瀏覽器支援下列 2 個函式 (全域)
		  *btoa : base64 Encode 
		  *atob : base64 Decode
		  *但是遇上中文會有編碼問題, 所以採用 b64EncodeUnicode 方法解決
		*/
		var imgsrc = 'data:image/svg+xml;base64,' + b64EncodeUnicode(html);
		/*
		var img = '<img src="'+imgsrc+'">'; 
		d3.select("#svgdataurl").html(img);
	  */
		//
		//var canvas = document.querySelector("canvas");
		var $svgContainer = $("#svg-useD3-container");
		var margin = 50;
		var canvasW = $svgContainer.width() + margin;
		var canvasH = $svgContainer.height() + margin;
		//console.log(canvasW);
		//console.log(canvasH);
		var canvas = document.createElement("canvas");
		canvas.setAttribute('width', canvasW);
		canvas.setAttribute('height', canvasH);
		context = canvas.getContext("2d");

		var downloadFileName = $("#planName").val();
		var image = new Image;
		image.src = imgsrc;
		image.onload = function () {
			context.drawImage(image, 0, 0);
			var canvasdata = canvas.toDataURL("image/jpg");
			var downloadLink = document.createElement("a");
			downloadLink.download = downloadFileName;
			downloadLink.href = canvasdata;
			downloadLink.click();
		};

	});
}//end

/*
 * 匯出 SVG
 */
function bindExportSVG() {
	d3.select("#exportToSVG").on("click", function () {
		//get svg element.
		var svg = document.getElementById("svg-useD3-container");

		//get svg source.
		var serializer = new XMLSerializer();
		var source = serializer.serializeToString(svg);

		//add name spaces.
		if (!source.match(/^<svg[^>]+xmlns="http\:\/\/www\.w3\.org\/2000\/svg"/)) {
			source = source.replace(/^<svg/, '<svg xmlns="http://www.w3.org/2000/svg"');
		}
		if (!source.match(/^<svg[^>]+"http\:\/\/www\.w3\.org\/1999\/xlink"/)) {
			source = source.replace(/^<svg/, '<svg xmlns:xlink="http://www.w3.org/1999/xlink"');
		}

		//add xml declaration
		source = '<?xml version="1.0" standalone="no"?>\r\n' + source;

		//convert svg source to URI data scheme.
		var url = "data:image/svg+xml;charset=utf-8," + encodeURIComponent(source);

		var downloadFileName = $("#planName").val();
		var downloadLink = document.createElement("a");
		downloadLink.download = downloadFileName;
		downloadLink.href = url;
		downloadLink.click();

		//set url value to a element's href attribute.
		//document.getElementById("exportToSVG").href = url;
		//you can download svg file by right click menu.
	});

}//end

/*
 * 匯出 SVG - 資料處理 (base64 編碼)
 */
function b64EncodeUnicode(str) {
	return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function (match, p1) {
		return String.fromCharCode('0x' + p1);
	}));
}
function b64DecodeUnicode(str) {
	return decodeURIComponent(atob(str).split('').map(function (c) {
		return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
	}).join(''));
}